from django.urls import path

from .views import homepage, detail, housedetail, getdivarhouse

urlpatterns = [
    path('', homepage, name='homepage'),
    path('housedetails/', housedetail, name='housedetail'),
    path('housedetail/v/<str:title>/<str:token>', detail, name='detail'),
    path('gethouse/', getdivarhouse, name='getdivarhouse'),
    
]
