from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import status
import requests
from bs4 import BeautifulSoup
import json
from selenium import webdriver


def homepage(request):
    return render(request, 'home.html')

def detail(request,title,token):
    ctx = {}
    ctx['house_url'] = "/v/"+title+"/"+token
    return render(request, 'detail.html', ctx)
     

@ api_view(['GET'])
def housedetail(request):
    url = request.GET.get('url')
    if url:
        return Response(gethousedetail(url),status=status.HTTP_200_OK)
    else:
        return Response({
            'status': False,
            'detail': 'ادرس پیدا نشد.'
        })

def gethousedetail(url):
    baseurl = 'https://divar.ir'
    house = requests.get(baseurl+url)
    soup = BeautifulSoup(house.text, 'html.parser')
    image=soup.find_all(class_='kt-image-block')
    y=[]
    z = {
        "description":soup.find(class_='kt-description-row__text--primary').string,
        "title":soup.find(class_='kt-page-title__title--responsive-sized').string,
        "subtitle":soup.find(class_='kt-page-title__subtitle--responsive-sized').string,
        "price":soup.find(class_='kt-unexpandable-row__value').string,
        "user":soup.find_all(class_='kt-unexpandable-row__value')[1].string,
        "phone":soup.find(class_='kt-unexpandable-row__action kt-text-truncate ltr').string,
    }
    y.append(z)
    for elm in image:
        x={
            "images":elm.find('img')['src'],
        }
        y.append(x) 
    return y

def gethouses(price,size,rooms,building_age,floor,user_type):
    baseurl = 'https://divar.ir/s/tehran/buy-apartment/shahrak-gharb?'
    house = requests.get(baseurl+"price="+str(price)+"&size="+str(size)+"&rooms="+str(rooms)+"&building-age="+str(building_age)+"&floor="+str(floor)+"&user_type="+str(user_type))
    soup = BeautifulSoup(house.text, 'html.parser')
    content = soup.find_all(class_='kt-post-card--bordered')
    y = []
    for elm in content:
        x = {
            "title":elm.find(class_='kt-post-card__title').string,
            "publish":elm.find(class_='kt-text-truncate').string,
            "price":elm.find(class_='kt-post-card__top-description').string,
            "image":elm.find('img')['src'],
            "url":elm['href'],
        }
        y.append(x) 
    return y    

@ api_view(['GET'])
def getdivarhouse(request):
    size = request.GET.get('size')
    price = request.GET.get('price')
    building_age = request.GET.get('building_age')
    rooms = request.GET.get('rooms')
    floor = request.GET.get('floor')
    user_type = request.GET.get('user_type')
    return Response(gethouses(price,size,rooms,building_age,floor,user_type),status=status.HTTP_200_OK)
    





